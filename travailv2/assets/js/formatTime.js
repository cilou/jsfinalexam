//returns 00:00:00
function convertMsToStr(nbMs) {
    //nbMs = Math.round(nbMs/1000)*1000;
    let time = new Date(nbMs);
    let hours = time.getUTCHours();
    let minutes = time.getMinutes();
    let seconds = time.getSeconds();

    let changingTimeStr = `${hours.toString().padStart(2, "0")}`;
    changingTimeStr += `:${minutes.toString().padStart(2, "0")}`;
    changingTimeStr += `:${seconds.toString().padStart(2, "0")}`;

    return changingTimeStr;
}

//returns 00:00:00.000
function convertMsToStrWithMs(nbMs) {
    let time = new Date(nbMs);
    let hours = time.getUTCHours();
    let minutes = time.getMinutes();
    let seconds = time.getSeconds();
    let mseconds = time.getMilliseconds();

    let changingTimeStr = `${hours.toString().padStart(2, "0")}`;
    changingTimeStr += `:${minutes.toString().padStart(2, "0")}`;
    changingTimeStr += `:${seconds.toString().padStart(2, "0")}`;
    changingTimeStr += `,${mseconds.toString().padStart(3, "0")}`;

    return changingTimeStr;
}
