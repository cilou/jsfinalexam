$('document').ready(function() {
    //*****************************************************************************
    const MAX_COURSE_NAME_LENGTH = 30;
    const MAX_COURSE_DESC_LENGTH = 100;
    const MIN_COURSE_DURATION_MS = 5000;
    const INTERVAL_DURATION_CHRONO_MS = 40;
    const PENALTY_INCOMPLETE_COURSE = 2;
    const PENALTY_ABANDON_COURSE = 10;
    const FILTER_ESPECES_ALL = 'ALL';

    const statusEnum = {"Inscrit": "I", "EnCourse": "C", "Arrive": "T", "Abandon": "A"};
    Object.freeze(statusEnum);
    // https://medium.com/@wlodarczyk_j/object-freeze-vs-object-seal-ba6d7553a436

    let course = new Object();
    let listAnimaux = null;

    let sectionCourse = $('#section-course');
    let txtCourseName = $('#txt-course-name');
    let listCourseCountry = $('#list-course-country');
    let txtCourseDuration = $('#txt-course-duration');
    let txtareaCourseDesc = $('#txtarea-course-desc');
    let btnCreateCourse = $('#btn-create-course');
    let fsCreateCourse = $('#fs-create-course');
    let fsInscriptions = $('#fs-inscriptions');

    // -----------------------------------------------------------------------------
    // MAIN
    // -----------------------------------------------------------------------------
    fsInscriptions.hide();
    btnCreateCourse.prop('disabled', true);
    loadSelectCountry();
    btnCreateCourse.on('click', createCourse);

    // -----------------------------------------------------------------------------
    // FUNCTIONS
    // -----------------------------------------------------------------------------

    //*******************************************************************
    //    PHASE 1 -- CREATION COURSE
    //*******************************************************************

    /*
     * Chargement des pays dans le select de la phase création de la course
     */
    function loadSelectCountry() {
        $.ajax({
            url: 'lib/rqListePays.php',
            method: 'GET',
            async: true,
            dataType: 'json'
        })
            .done(function(data) {
                for(pays of data) {
                    let option = $('<option>', {
                        codeP: pays.codeP,
                        text: pays.nomP
                    });
                    listCourseCountry.append(option);
                }
                btnCreateCourse.prop('disabled', false);
            })
            .fail(function(xhr, status) {
                console.log(xhr.statusText, status);
            })
            .always(function() {
                console.log('Req Liste Pays completed');
            })
    }

    /*
     * Crée la course dans la DB après validation des données  et
     * crée l'interface d'inscription à cette course
     */
    function createCourse() {
        // Supprime les erreurs éventuelles affichées
        let errors = sectionCourse.find('.error');
        for(error of errors) {
            $(error).empty();
        }

        // Validation des données avant de continuer
        if(isCourseDataValid()) {

            // Charge la liste des animaux dans le select des inscriptions (async donc on le start
            // déjà vu qu'on sait que les données entrées sont valides)
            loadAnimauxData();

            // Attribution des valeurs de la course à l'objet course
            // Limite le nombre de charactères insérés dans la DB pour certains champs
            course.nomC = (txtCourseName.val().trim()).substr(0, MAX_COURSE_NAME_LENGTH);
            course.descC = (txtareaCourseDesc.val().trim()).substr(0, MAX_COURSE_DESC_LENGTH);
            let optionSelected = listCourseCountry.find('option:selected');
            course.lieuC = optionSelected.attr('codeP');
            course.nomPays = optionSelected.text();
            course.dateC = dateObjectToEnglishFormat(new Date());

            // Insertion de la course dans la DB
            $.ajax({
                url: 'lib/rqInsertCourse.php',
                method: 'POST',
                dataType: 'json',
                data: {course: JSON.stringify(course)}
            })
            .done(function(lastInsertedId) {
                if(lastInsertedId === 0) {
                    btnCreateCourse.siblings('.error')
                        .text("Une erreur s'est produite lors de l'insertion des données. Veuillez réessayer plus tard.");
                } else {
                    course.id = lastInsertedId;

                    txtCourseName.prop('disabled', true);
                    listCourseCountry.prop('disabled', true);
                    txtCourseDuration.prop('disabled', true);
                    txtareaCourseDesc.prop('disabled', true);
                    btnCreateCourse.prop('disabled', true);

                    // Création de l'interface inscriptions (phase 2)
                    fsInscriptions.append(createInscriptionInterface());
                    fsCreateCourse.remove();
                    fsInscriptions.show();
                }
            })
            .fail(function(xhr, status) {
                console.log(xhr, status);
            })
            .always(function(){
                console.log('Insert Course completed');
            });
        }
    }

    /*
     * Validation des données de la création de course
     */
    function isCourseDataValid() {
        let error = false;
        if((txtCourseName.val().trim()).length === 0) {
            txtCourseName.siblings('.error').text('Le nom de la course ne peut être vide');
            error = true;
        }

        if(listCourseCountry.find('option:selected').attr('codeP') === undefined) {
            listCourseCountry.siblings('.error').text('Un pays doit obligatoirement être sélectionné');
            error = true;
        }

        if((txtCourseDuration.val().trim()).length === 0) {
            txtCourseDuration.siblings('.error').text('La durée de la course ne peut être vide');
            error = true;
        } else {
            let durationString = txtCourseDuration.val();
            let pattern = new RegExp('[0-9][0-9]:[0-5][0-9]:[0-5][0-9]');
            if(!durationString.match(pattern)) {
                txtCourseDuration.siblings('.error').text('La durée de la course doit respecter le format 99:59:59');
                error = true;
            } else {
                let arrayTime = durationString.split(':');
                let courseDurationMs =
                    Number(arrayTime[0])*60*60*1000 +
                    Number(arrayTime[1])*60*1000 +
                    Number(arrayTime[2]*1000);
                if(courseDurationMs < MIN_COURSE_DURATION_MS) {
                    txtCourseDuration.siblings('.error')
                        .text(`La durée de la course doit être au minimum de ${MIN_COURSE_DURATION_MS/1000} secondes`);
                    error = true;
                } else {
                    course.durationMs = courseDurationMs;
                }
            }
        }

        return !error;
    }

    //*******************************************************************
    //    PHASE 2 -- CREATION INSCRIPTIONS + CLOTURE ET DEMARRAGE COURSE
    //*******************************************************************

    /*
     * Chargement des animaux dans l'object global listAnimaux
     */
    function loadAnimauxData() {
        $.ajax({
            url: 'lib/rqListeAnimaux.php',
            method: 'GET',
            dataType: 'json',
            async: false // parce qu'on en a absolument besoin avant de créer la course dans la DB
        })
        .done(function(animaux) {
            listAnimaux = animaux;
        })
        .fail(function(xhr, status) {
            console.log(xhr, status);
        })
        .always(function() {
            console.log('Load listAnimaux completed');
        })
    }

    /*
     * Création de l'interface Inscriptions
     */
    function createInscriptionInterface() {

        fsInscriptions.append(createCourseDetails());
        fsInscriptions.append(createCourseFilters());

        let divInscriptionsCourse = $('<div>', {id: 'div-inscriptions-course'});
        divInscriptionsCourse.append(createSelectAnimaux());

        // Bouton d'inscription d'un animal
        let btnInscrireAnimal = $('<button>', {
            text: 'Inscrire',
            id: 'btn-inscrire-animal',
            disabled: true
        });

        btnInscrireAnimal.on('click', function() {
            selectAnimaux = fsInscriptions.find('#select-animaux');
            let selectedAnimal = selectAnimaux.find('option:selected');
            if(selectedAnimal.attr('idA') !== undefined) {
                let animal = listAnimaux.find(k => k.idA === selectedAnimal.attr('idA'));
                let divInscriptionsCourse = fsInscriptions.find('#div-inscriptions-course');
                inscrireAnimal(animal, divInscriptionsCourse);
                selectedAnimal.remove();
                fsInscriptions.find('#btn-cloturer-inscriptions').prop('disabled', false);

                // Si tous les animaux de la liste ont été sélectionnés, on  désactive le
                // bouton inscrire et on affiche un message dans la liste déroulante
                if(selectAnimaux.find('option:selected').length === 0) {
                    selectAnimaux.siblings('#btn-inscrire-animal').prop('disabled', true);
                    selectAnimaux.empty();
                    let option = $('<option>', {
                        text: 'La liste est vide...',
                        disabled: true,
                        selected: true
                    });

                    selectAnimaux.append(option);
                }
            }
        });
        // END Bouton d'inscription d'un animal

        // Bouton de cloture des inscriptions à la course
        let btnCloturerInscriptions = $('<button>', {
            text: 'Cloturer les inscriptions',
            id: 'btn-cloturer-inscriptions',
            disabled: true
        });

        // Click sur btnCloturerInscriptions
        btnCloturerInscriptions.on('click', function() {
            fsInscriptions.find('legend').text('Inscriptions clôturées');
            let divFilters = fsInscriptions.find('#div-filters-course');
            divFilters.remove();

            let animauxInscrits = fsInscriptions.find('#div-inscriptions-course .participant');

            // Bouton de démarrage de la course
            let btnCourseStart = $('<button>', {
                type: 'button',
                id: 'btn-course-start',
                text: 'Start',
                nbMs: course.durationMs
            });

            btnCourseStart.on('click', function() {
                fsInscriptions.find('legend').text('En course');
                course.remainingMs = course.durationMs;
                fsInscriptions.find('#div-inscriptions-course .participant').attr('statut', statusEnum.EnCourse);
                course.startDate = new Date();
                course.timer = setInterval(function() {
                    course.remainingMs = course.durationMs - (new Date(new Date() - course.startDate).getTime());
                    if(course.remainingMs < 0) course.remainingMs = 0; // sinon affichage #txt > course.durationMs
                    fsInscriptions.find('#pg-course-remaining-time').val(course.remainingMs);
                    //fsInscriptions.find('#pg-course-remaining-time').attr('title', updateTime(course.remainingMs));
                    fsInscriptions.find('#txt-course-start-time').val(updateTime(course.durationMs - course.remainingMs));
                    checkIfCourseIsFinished(course.remainingMs);
                }, INTERVAL_DURATION_CHRONO_MS);
                $(this).prop('disabled', true);
                fsInscriptions.find('.btn-course-stop').prop('disabled', false);
            });
            // END Bouton de démarrage de la course

            $(this).siblings('#select-animaux').before(btnCourseStart);
            $(this).siblings('#select-animaux').remove();

            let txtCourseStartTime = $('<input>', {
                type: 'text',
                id: 'txt-course-start-time',
                value: '00:00:00.000',
                nbMs: 0,
                readonly: true
            });
            $(this).siblings('#btn-inscrire-animal').before(txtCourseStartTime);
            $(this).siblings('#btn-inscrire-animal').remove();

            let lblCourseRemainingTime = $('<label>', {
                for: 'txt-course-remaining-time',
                text: 'Temps restant'
            });

            let pgCourseRemainingTime = $('<progress>', {
                id: 'pg-course-remaining-time',
                value: course.durationMs,
                max: course.durationMs,
                min: 0
            });

            $(this).before(lblCourseRemainingTime);
            $(this).before(pgCourseRemainingTime);
            $(this).remove();

            $(this).prop('disabled', true);

            //On crée les containers de statuts d'arrivée pour le 'podium'
            let divArrive = $('<div>', {id: 'divArrive'});
            let divIncomplet = $('<div>', {id: 'divIncomplet'});
            let divAbandon = $('<div>', {id: 'divAbandon'});
            pgCourseRemainingTime.after(divArrive);
            divArrive.after(divIncomplet);
            divIncomplet.after(divAbandon);

            // On crée le STOP button template
            let btnStop = $('<button>', {
                type: 'button',
                class: 'btn-course-stop',
                text: 'STOP',
                disabled: true
            });
            btnStop.on('click', function() {
                let txtCourseTime = fsInscriptions.find("#txt-course-start-time");
                let lbl = $('<label>', {
                    text: txtCourseTime.val(),
                    nbMs: course.durationMs - course.remainingMs
                });
                $(this).before(lbl);
                $(this).siblings('.btn-course-abandon').remove();
                let participant = $(this).closest('.participant');
                participant.removeClass('running');
                if(participant.hasClass('incomplete')) {
                    $(this).remove();
                    // On déplace le participant dans la div des participants qui n'ont pas terminé
                    participant.attr('statut', statusEnum.Arrive);
                    participant.find('.lgAnimal').append($('<span>', {'text': 'Pas Terminé', 'class': 'participant-resultat'}));
                    fsInscriptions.find('#divIncomplet').append(participant.clone());
                } else {
                    participant.addClass('stop');
                    participant.attr('statut', statusEnum.Arrive);
                    participant.find('.lgAnimal').append($('<span>', {'text': 'Arrivé', 'class': 'participant-resultat'}));
                    $(this).remove();
                    // On déplace le participant dans la div des participants arrivés
                    fsInscriptions.find('#divArrive').append(participant.clone());
                }

                participant.remove();
                if(fsInscriptions.find('.running').length === 0) {
                    checkIfCourseIsFinished(0);
                    //console.log('La course est terminée');
                }
            });
            // END On crée le STOP button template

            // On crée le Abandon button template
            let btnAbandon = $('<button>', {
                type: 'button',
                class: 'btn-course-abandon',
                text: 'Abandon'
            });
            btnAbandon.on('click', function() {
                let lbl = $('<label>', {
                    text: convertMsToStrWithMs(course.durationMs* PENALTY_ABANDON_COURSE),
                    nbMs: course.durationMs * PENALTY_ABANDON_COURSE
                });
                $(this).before(lbl);
                $(this).siblings('.btn-course-stop').remove();
                let participant = $(this).closest('.participant');
                participant.removeClass('running');
                participant.addClass('abandon');
                participant.attr('statut', statusEnum.Abandon);
                participant.find('.lgAnimal').append($('<span>', {'text': 'Abandon', 'class': 'participant-resultat'}));
                $(this).remove();
                // On déplace le participant dans la div des participants qui sont en abandon (administratif ? ^^)
                fsInscriptions.find('#divAbandon').append(participant.clone());
                participant.remove();
                if(fsInscriptions.find('.running').length === 0) {
                    checkIfCourseIsFinished(0);
                    //si 100% abandon avant commencement course
                    fsInscriptions.find('#btn-course-start').prop('disabled', true);
                    //console.log('La course est terminée');
                }
            });
            // END On crée le Abandon button template

            // On ajoute des clones des templates 'STOP' et 'Abandon' créés et on supprime le bouton supprimer
            for(animalInscrit of animauxInscrits) {
                $(animalInscrit).addClass('running');
                let btnSupprInscription = $(animalInscrit).find('.btn-supprimer-inscription');
                btnSupprInscription.before(btnStop.clone(true));
                btnSupprInscription.before(btnAbandon.clone(true));
                btnSupprInscription.remove();
            }
        }); // END Click on btnCloturerInscriptions
        // END Bouton de cloture des inscriptions à la course

        divInscriptionsCourse.append(btnInscrireAnimal);
        divInscriptionsCourse.append(btnCloturerInscriptions);

        return divInscriptionsCourse;
    }

    /*
     * Affichage des détails de la course
     */
    function createCourseDetails() {
        let divDetailsCourse = $('<div>', {
            id: 'div-details-course',
            duration: txtCourseDuration.val(),
            idC: course.id
        });
        divDetailsCourse.append($('<p>', {text: 'nom: ' + txtCourseName.val()}));
        divDetailsCourse.append($('<p>', {text: 'pays: ' + listCourseCountry.val()}));
        divDetailsCourse.append($('<p>', {text: 'durée: ' + txtCourseDuration.val()}));
        //divDetailsCourse.append($('<p>', {text: 'description: ' + txtareaCourseDesc.val()}));

        return divDetailsCourse;
    }

    /*
     * Creation du select avec la liste des animaux
     */
    function createSelectAnimaux() {
        let selectAnimaux = $('<select>', {
            id: 'select-animaux'
        });

        loadSelectAnimaux(selectAnimaux);

        // Active le bouton inscription lorsqu'un animal est sélectionné
        // Ne devrait être fait qu'une seule fois car, au chargement, la liste est sur
        // sélectionnez un animal...
        selectAnimaux.on('change', function() {
            let btnInscrireAnimal = fsInscriptions.find('#btn-inscrire-animal');
            btnInscrireAnimal.prop('disabled', false);
        });

        return selectAnimaux;
    }

    /*
     * Chargement du select avec les animaux depuis l'objet listAnimaux
     *  Si filtre(s) sur l'espèce, on ne charge que les anilaux de cette ou ces espèces
     */
    function loadSelectAnimaux(selectAnimaux, especes=[]) {
        selectAnimaux.siblings('#btn-inscrire-animal').prop('disabled', true);
        selectAnimaux.empty();

        let option = $('<option>', {
            text: 'Veuillez sélectionner un animal...',
            disabled: true,
            selected: true
        });

        selectAnimaux.append(option);

        for(animal of listAnimaux) {
            //si l'animal est déjà inscrit à la course, on l'ignore
            if(animal.registered !== undefined) continue;
            if(especes.length === 0 || (especes.length > 0 && especes.indexOf(animal.espA) > -1)) {
                option = $('<option>', {
                    idA: animal.idA,
                    nomA: animal.nomA,
                    descA: animal.descA,
                    espA: animal.espA,
                    nomE: animal.nomE,
                    nbPattesE: animal.nbPattesE,
                    nationA: animal.nationA,
                    nomP: animal.nomP,
                    text: animal.nomA + ' : ' +
                        animal.nomE +
                        (animal.nomP === null ? '' : ', ' + animal.nomP)
                });

                selectAnimaux.append(option);
            }
        }
        // S'il n'y a qu'une option dans la liste, c'est le 'label' => la liste est vide
        if(selectAnimaux.find('option').length === 1) {
            selectAnimaux.empty();
            let option = $('<option>', {
                text: 'La liste est vide...',
                disabled: true,
                selected: true
            });
            selectAnimaux.append(option);
        }
    }

    /*
     * Chargement des espèces listées dans la liste des animaux
     */
    function loadSelectEspeces(selectEspeces) {
        let mapEspeces = new Map();
        for(animal of listAnimaux) {
            //si l'animal est déjà inscrit à la course, on l'ignore
            if(animal.registered === undefined)
                mapEspeces.set(animal.espA, animal.nomE);
        }
        for(let[key, value] of mapEspeces) {
            option = $('<option>', {
                espA: key,
                nomE: value,
                text: value,
                value: key
            });

            selectEspeces.append(option);
        }
    }

    /*
     * Création des filtres su la sélection d'animaux participants à la course
     * Filtre actuel sur les espèces participantes
     */
    function createCourseFilters() {
        let divFiltersCourse = $('<div>', {
            id: 'div-filters-course',
            duration: txtCourseDuration.val(),
            idC: course.id
        });

        let divFiltersCourseEspeces = $('<div>', {
            id: 'div-filters-course-especes'
        });

        let lblSelectEspeces = $('<label>', {
            for: 'select-especes',
            text: 'Filtrer la liste des animaux par espece'
        });

        let selectEspeces = $('<select>', {
            id: 'select-especes',
            multiple: true
        });
        let option = $('<option>', {
            text: 'Toutes',
            value: FILTER_ESPECES_ALL,
            selected: true
        });

        selectEspeces.append(option);
        loadSelectEspeces(selectEspeces);

        let btnFilterEspeces = $('<button>', {
            id: 'btn-filter-especes',
            text: 'Filtrer'
        });

        btnFilterEspeces.on('click', function() {
            let selectedCollection = $(this).siblings('#select-especes').find('option:selected');
            if(selectedCollection.filter('option[value="' + FILTER_ESPECES_ALL + '"]').length > 0) {
                loadSelectAnimaux(fsInscriptions.find('#select-animaux'));
            } else {
                let espArray = [];
                for(selected of selectedCollection) {
                    espArray.push($(selected).val());
                }
                loadSelectAnimaux(fsInscriptions.find('#select-animaux'), espArray);
            }
        });

        divFiltersCourseEspeces.append(lblSelectEspeces);
        divFiltersCourseEspeces.append(selectEspeces);
        divFiltersCourseEspeces.append(btnFilterEspeces);
        divFiltersCourse.append(divFiltersCourseEspeces);

        return divFiltersCourse;
    }

    /*
     * Inscription d'un animal à la course
     */
    function inscrireAnimal(animal, container) {
        let index = listAnimaux.indexOf(animal);
        if(index === -1) {
            throw 'inscrireAnimal -- Animal not found in listAnimaux ! This should not happen !';
        }
        // On indique que l'animal à cet index est inscrit pour ne pas le re-sélectionner (filtres)
        listAnimaux[index].registered = true;

        let btnSupprimerInscription = $('<button>', {
            type: 'button',
            text: 'Supprimer',
            class: 'btn-supprimer-inscription'
        });

        // Si l'inscription d'un animal est supprimée, on le rajoute au select
        btnSupprimerInscription.on('click', function() {
            let idA = $(this).closest('div').attr('idA');
            let currentAnimal = listAnimaux.find(k => k.idA === idA);
            let index = listAnimaux.indexOf(currentAnimal);
            if(index === -1) {
                throw 'btnSupprimerInscription -- Animal not found in listAnimaux ! This should not happen !';
            }
            // On indique que l'animal à cet index n'est pas inscrit pour pouvoir le sélectionner (filtres)
            listAnimaux[index].registered = undefined;
            // On recharge la liste en tenant compte d'éventuels filtres en cours
            fsInscriptions.find('#btn-filter-especes').trigger("click");
            // On supprime le container de l'animal à désincrire
            $(this).closest('.participant').remove();
            // On désactive le bouton 'clôturer inscription' si aucun animal n'est inscrit
            let divInscriptionsCourse = fsInscriptions.find('#div-inscriptions-course');
            if(divInscriptionsCourse.find('.participant').length === 0) {
                divInscriptionsCourse.find('#btn-cloturer-inscriptions').prop('disabled', true);
            }
        });

        // On crée le container de l'animal qui contiendra toutes les infos de l'animal en question
        let divAnimal = $('<div>', {
            idA: animal.idA,
            statut: statusEnum.Inscrit,
            class: 'participant'
        });
        let pAnimal = $('<p>', {'class': 'lgAnimal'});
        pAnimal.append(btnSupprimerInscription);
        pAnimal.append($('<span>', {text: animal.idA}));
        pAnimal.append($('<span>', {text: animal.nomA}));
        pAnimal.append($('<span>', {text: animal.nomP}));
        pAnimal.append($('<span>', {text: animal.descA}));
        pAnimal.append($('<span>', {text: animal.nomE}));

        divAnimal.append(btnSupprimerInscription);
        divAnimal.append(pAnimal);
        container.append(divAnimal);
    }

    //*******************************************************************
    //    PHASE 3 -- Suivi du temps + gestion fin de course
    //*******************************************************************

    /*
     * Utilisé pour mettre à jour l'affichage su temps passé depuis le
     * démarrage de la course
     */
    function updateTime(nbMs) {
        changingTimeStr = convertMsToStrWithMs(nbMs);
        checkIfCourseIsFinished(nbMs);

        return changingTimeStr;
    }

    /*
     * Si le temps est à zéro, on reset, finalise et persiste
     */
    function checkIfCourseIsFinished(nbMs) {
        // la course n'est pas terminée ou le timer a déjà été resetté
        if(nbMs > 0 || course.timer === null) return;

        // La course est terminée
        clearInterval(course.timer);
        course.timer = null;
        fsInscriptions.find('legend').text('Course terminée -- Résultats');

        // On check s'il y a encore des participants en course et on les arrête
        // en leur infligeant un 'penalty'
        let participantsDivsStillRunning = fsInscriptions.find('.running');
        for(participant of participantsDivsStillRunning) {
            // $(participant).find('.btn-course-stop').trigger("click");
            $(participant).removeClass('stop');
            $(participant).addClass('incomplete');
            $(participant).find('label').attr('nbms', course.durationMs*PENALTY_INCOMPLETE_COURSE);
            $(participant).find('label').text(convertMsToStrWithMs(Number(course.durationMs*PENALTY_INCOMPLETE_COURSE)));
            $(participant).attr('statut', statusEnum.Arrive);
            $(participant).find('.lgAnimal').children().last().text('Pas terminé');
            $(participant).find('.btn-course-stop').trigger("click");
        }

        //On persiste les résultats de la course
        insertResultsIntoDatabase();
    }

    function insertResultsIntoDatabase() {
        let participantsDivs = fsInscriptions.find('.participant');
        for(participant of participantsDivs) {
            let resultat = new Object();
            resultat.idC = course.id;
            resultat.idA = $(participant).attr('idA');
            // On arrondit la seconde à l'unité sup si partie ms >= 500 (stockage DB sans les ms)
            resultat.temps = convertMsToStr(Number($(participant).find('label').attr('nbms'))+500);
            //resultat.temps = convertMsToStrWithMs(Number($(participant).find('label').attr('nbms')));
            resultat.statut = $(participant).attr('statut');
            // if tps course % 1000 > 0 => add 1 otherwise course time might be 0 seconds ??

            $.ajax({
                url: 'lib/rqInsertResultat.php',
                method: 'POST',
                data: {resultat: JSON.stringify(resultat)},
                dataType: 'json',
                async: true
            })
            .done(function(rowCount) {
                if(rowCount === 0) {
                    console.log(`Error inserting course result for ${JSON.stringify(resultat)}`);
                } else {
                    console.log('Insertion successful');
                }
            })
            .fail(function(xhr, status) {
                console.log(console.log(xhr, status));
            })
            .always(function(){
                console.log('Insert new result completed');
            })
        }
    }

    //*****************************************************************************
});