$('document').ready(function() {
    //*****************************************************************************
    const MAX_COURSE_NAME_LENGTH = 30;
    const MAX_COURSE_DESC_LENGTH = 100;
    const MIN_COURSE_DURATION_MS = 5000;
    const INTERVAL_DURATION_CHRONO_MS = 40;
    const PENALTY_INCOMPLETE_COURSE = 2;

    let statusEnum = {"Inscrit": "I", "EnCourse": "C", "Arrive": "T", "Abandon": "A"};
    Object.freeze(statusEnum);

    let timeOut = null;
    let course = new Object();
    let listEspeces = null;
    let listAnimaux = null;

    let sectionCourse = $('#section-course');
    let txtCourseName = $('#txt-course-name');
    let listCourseCountry = $('#list-course-country');
    let txtCourseDuration = $('#txt-course-duration');
    let txtareaCourseDesc = $('#txtarea-course-desc');
    let btnCreateCourse = $('#btn-create-course');
    let fsCreateCourse = $('#fs-create-course');
    let fsInscriptions = $('#fs-inscriptions');

    // -----------------------------------------------------------------------------
    // MAIN
    // -----------------------------------------------------------------------------
    fsInscriptions.hide();
    btnCreateCourse.prop('disabled', true);
    loadCountryList();
    btnCreateCourse.on('click', createCourse);
    // let errorOnLoading = false;
    // checkIfAnimauxIsLoaded(10);
    // if(errorOnLoading) {
    //     //todo Program cannot continue listeAnimaux cannot be loaded
    //     console.log("Program cannot continue listeAnimaux cannot be loaded");
    //     return 125;
    // } else {
    //     console.log("Program continues");
    // }


    // -----------------------------------------------------------------------------
    // FUNCTIONS
    // -----------------------------------------------------------------------------

    // function checkIfAnimauxIsLoaded(maxRetries) {
    //     if(listAnimaux === null && maxRetries > 0) {
    //         console.log('ListAnimaux still not loaded');
    //         timeOut = setTimeout(function() {
    //             checkIfAnimauxIsLoaded(maxRetries-1);
    //         }, 100);
    //         errorOnLoading = true;
    //     } else if(listAnimaux != null) {
    //         clearTimeout(timeOut);
    //         console.log('ListAnimaux loaded, maxRetries value is ' + maxRetries);
    //         errorOnLoading = false;
    //         //console.log('listeAnimaux: ' + listAnimaux);
    //     } else {
    //         clearTimeout(timeOut);
    //         console.log('ListAnimaux not loaded after 1 second');
    //         errorOnLoading = true;
    //     }
    // }

    function loadAnimauxData() {
        // listAnimaux = $.ajax({
        //     url: 'lib/rqListeAnimaux.php',
        //     method: 'GET',
        //     dataType: 'json',
        //     async: true
        // }).responseJSON;

        $.ajax({
            url: 'lib/rqListeAnimaux.php',
            method: 'GET',
            dataType: 'json',
            async: true
        })
        .done(function(animaux) {
            listAnimaux = animaux;
            //console.log('listeAnimaux: ' + JSON.stringify(listAnimaux));
            //console.log('length: ' + listAnimaux.length);
        })
        .fail(function(xhr, status) {
            console.log(xhr, status);
        })
        .always(function() {
            console.log('Load listAnimaux completed');
        })
    }

    function createCourse(e) {
        //e.preventDefault();
        let errors = sectionCourse.find('.error');
        for(error of errors) {
            $(error).empty();
        }

        if(isCourseDataValid()) {

            loadAnimauxData();

            course.nomC = (txtCourseName.val().trim()).substr(0, MAX_COURSE_NAME_LENGTH);
            course.descC = (txtareaCourseDesc.val().trim()).substr(0, MAX_COURSE_DESC_LENGTH);
            // todo optimize
            course.lieuC = listCourseCountry.find('option:selected').attr('codeP');
            course.nomPays = listCourseCountry.find('option:selected').text();
            course.dateC = dateObjectToEnglishFormat(new Date());

            $.ajax({
               url: 'lib/rqInsertCourse.php',
               method: 'POST',
               dataType: 'json',
               data: {course: JSON.stringify(course)}
            })
            .done(function(lastInsertedId) {
                if(lastInsertedId === 0) {
                    btnCreateCourse.siblings('.error').text("Une erreur s'est produite lors de l'insertion des données. Veuillez réessayer plus tard.");
                } else {
                    course.id = lastInsertedId;

                    txtCourseName.prop('disabled', true);
                    listCourseCountry.prop('disabled', true);
                    txtCourseDuration.prop('disabled', true);
                    txtareaCourseDesc.prop('disabled', true);
                    btnCreateCourse.prop('disabled', true);

                    let divDetailsCourse = $('<div>', {
                        id: 'div-details-course',
                        duration: txtCourseDuration.val(),
                        idC: course.id
                    });
                    divDetailsCourse.append($('<p>', {text: 'nom: ' + txtCourseName.val()}));
                    divDetailsCourse.append($('<p>', {text: 'pays: ' + listCourseCountry.val()}));
                    divDetailsCourse.append($('<p>', {text: 'durée: ' + txtCourseDuration.val()}));
                    //divDetailsCourse.append($('<p>', {text: 'description: ' + txtareaCourseDesc.val()}));
                    fsInscriptions.append(divDetailsCourse);

                    let divInscriptionsCourse = $('<div>', {id: 'div-inscriptions-course'});

                    // --------------------------
                    // Create select list animaux
                    // --------------------------
                    let selectAnimaux = $('<select>', {
                        id: 'select-animaux'
                    });
                    let option = $('<option>', {
                        text: 'Veuillez sélectionner un animal...',
                        disabled: true,
                        selected: true
                    });

                    selectAnimaux.append(option);

                    for(animal of listAnimaux) {
                        //console.log(animal);
                        option = $('<option>', {
                            idA: animal.idA,
                            nomA: animal.nomA,
                            descA: animal.descA,
                            espA: animal.espA,
                            nomE: animal.nomE,
                            nbPattesE: animal.nbPattesE,
                            nationA: animal.nationA,
                            nomP: animal.nomP,
                            text: animal.nomA + ' : ' +
                                animal.nomE +
                                (animal.nomP === null ? '' : ', ' + animal.nomP)
                        });

                        selectAnimaux.append(option);
                    }
                    selectAnimaux.on('change', function() {
                        let btnInscrireAnimal = fsInscriptions.find('#btn-inscrire-animal');
                        btnInscrireAnimal.prop('disabled', false);
                    });
                    // ------------------------------
                    // END Create select list animaux
                    // ------------------------------

                    divInscriptionsCourse.append(selectAnimaux);

                    let btnInscrireAnimal = $('<button>', {
                        text: 'Inscrire',
                        id: 'btn-inscrire-animal',
                        disabled: true
                    });

                    btnInscrireAnimal.on('click', function() {
                        selectAnimaux = fsInscriptions.find('#select-animaux');
                        let selectedAnimal = selectAnimaux.find('option:selected');
                        if(selectedAnimal.attr('idA') !== undefined) {
                            let animal = listAnimaux.find(k => k.idA === selectedAnimal.attr('idA'));
                            console.log(animal);
                            inscrireAnimal(animal, divInscriptionsCourse);
                            selectedAnimal.remove();
                            fsInscriptions.find('#btn-cloturer-inscriptions').prop('disabled', false);
                        }
                    });

                    let btnCloturerInscriptions = $('<button>', {
                        text: 'Cloturer les inscriptions',
                        id: 'btn-cloturer-inscriptions',
                        disabled: true
                    });

                    // Click on btnCloturerInscriptions
                    btnCloturerInscriptions.on('click', function() {
                        let registeredAnimalDivs = fsInscriptions.find('#div-inscriptions-course').children('div');

                        let btnCourseStart = $('<button>', {
                            type: 'button',
                            id: 'btn-course-start',
                            text: 'Start',
                            nbMs: course.durationMs
                        });

                        btnCourseStart.on('click', function() {
                           course.remainingMs = course.durationMs;
                           fsInscriptions.find('#div-inscriptions-course').children('div[statut]').attr('statut', statusEnum.EnCourse);
                           course.timer = setInterval(function() {
                               course.remainingMs -= INTERVAL_DURATION_CHRONO_MS;
                               fsInscriptions.find('#txt-course-remaining-time').val(updateTime(course.remainingMs));
                               fsInscriptions.find('#txt-course-start-time').val(updateTime(course.durationMs - course.remainingMs));
                           }, INTERVAL_DURATION_CHRONO_MS);
                           $(this).prop('disabled', true);
                        });

                        $(this).siblings('#select-animaux').before(btnCourseStart);
                        $(this).siblings('#select-animaux').remove();

                        let txtCourseStartTime = $('<input>', {
                            type: 'text',
                            id: 'txt-course-start-time',
                            value: '00:00:00.000',
                            nbMs: 0,
                            readonly: true
                        });
                        $(this).siblings('#btn-inscrire-animal').before(txtCourseStartTime);
                        $(this).siblings('#btn-inscrire-animal').remove();

                        let lblCourseRemainingTime = $('<label>', {
                            for: 'txt-course-remaining-time',
                            text: 'Temps restant'
                        });
                        let txtCourseRemainingTime = $('<input>', {
                            type: 'text',
                            id: 'txt-course-remaining-time',
                            value: updateTime(course.durationMs),
                            nbMs: course.durationMs,
                            readonly: true
                        });

                        $(this).before(lblCourseRemainingTime);
                        $(this).before(txtCourseRemainingTime);
                        $(this).remove();

                        $(this).prop('disabled', true);

                        // We create the STOP button template
                        let btnStop = $('<button>', {
                            type: 'button',
                            class: 'btn-course-stop',
                            text: 'STOP'
                        });
                        btnStop.on('click', function() {
                           let txtCourseTime = fsInscriptions.find("#txt-course-start-time");
                           let lbl = $('<label>', {
                              text: txtCourseTime.val(),
                              //nbMs: txtCourseTime.attr('nbMs')
                              nbMs: course.durationMs - course.remainingMs
                           });
                           $(this).before(lbl);
                           $(this).siblings('.btn-course-abandon').remove();
                           $(this).closest('div').removeClass('running');
                           $(this).closest('div').addClass('stop');
                           $(this).closest('div').attr('statut', statusEnum.Arrive);
                           $(this).remove();
                            if(fsInscriptions.find('.running').length === 0) {
                                updateTime(0);
                                console.log('Course is finished');
                            }
                        });

                        // We create the Abandon button template
                        let btnAbandon = $('<button>', {
                            type: 'button',
                            class: 'btn-course-abandon',
                            text: 'Abandon'
                        });
                        btnAbandon.on('click', function() {
                            let lbl = $('<label>', {
                                text: '00:00:00.000',
                                nbMs: 0
                            });
                            $(this).before(lbl);
                            $(this).siblings('.btn-course-stop').remove();
                            $(this).closest('div').removeClass('running');
                            $(this).closest('div').addClass('abandon');
                            $(this).closest('div').attr('statut', statusEnum.Abandon);
                            $(this).remove();
                            if(fsInscriptions.find('.running').length === 0) {
                                updateTime(0);
                                console.log('Course is finished');
                            }
                        });

                        // We add clones of the button templates created above and we remove the 'Supprimer' button
                        for(div of registeredAnimalDivs) {
                            $(div).addClass('running');
                            $(div).find('.btn-supprimer-inscription').before(btnStop.clone(true));
                            $(div).find('.btn-supprimer-inscription').before(btnAbandon.clone(true));
                            $(div).find('.btn-supprimer-inscription').remove();
                        }
                    }); // END Click on btnCloturerInscriptions

                    divInscriptionsCourse.append(btnInscrireAnimal);
                    divInscriptionsCourse.append(btnCloturerInscriptions);
                    fsInscriptions.append(divInscriptionsCourse);

                    fsCreateCourse.remove();
                    fsInscriptions.show();
                }
            })
            .fail(function(xhr, status) {
                console.log(xhr, status);
            })
            .always(function(){
                console.log('Insert Course completed');
            });
        }
    }

    function updateTime(nbMs) {
        // let time = new Date(nbMs);
        // let hours = time.getUTCHours();
        // let minutes = time.getMinutes();
        // let seconds = time.getSeconds();
        // let mseconds = time.getMilliseconds();
        //
        // let changingTimeStr = `${hours.toString().padStart(2, "0")}`;
        // changingTimeStr += `:${minutes.toString().padStart(2, "0")}`;
        // changingTimeStr += `:${seconds.toString().padStart(2, "0")}`;
        // changingTimeStr += `,${mseconds.toString().padStart(3, "0")}`;

        changingTimeStr = convertMsToStrWithMs(nbMs);

        if(nbMs <= 0) {
            clearInterval(course.timer);
            //let runningAnimals = fsInscriptions.find('.running');
            //console.log(runningAnimals);
            //for(runningAnimal of runningAnimals) {
            //    $(runningAnimal).attr('statut', statusEnum.EnCourse);
            //}
            let txtCourseTime = fsInscriptions.find("#txt-course-start-time");
            let participantsDivsStillRunning = fsInscriptions.find('.running');
            for(participant of participantsDivsStillRunning) {
                $(participant).find('.btn-course-stop').trigger("click");
                // console.log('LOOP ', $(participant).find('#btn-course-stop'));
                // let lbl = $('<label>', {
                //     text: txtCourseTime.val(),
                //     //nbMs: txtCourseTime.attr('nbMs')
                //     nbMs: course.durationMs * PENALTY_INCOMPLETE_COURSE
                // });
                // $(participant).find('.btn-course-stop').before(lbl);
                // $(participant).find('.btn-course-stop').remove();
                // $(participant).find('.btn-course-abandon').remove();
                $(participant).find('label').attr('nbms', course.durationMs*PENALTY_INCOMPLETE_COURSE);
                $(participant).attr('statut', statusEnum.Arrive);
            }
            //fsInscriptions.find('.running').attr('statut', statusEnum.Abandon);
            let participantsDivs = fsInscriptions.find('.participant');
            //let idC = course.id;
            //let nbMs = course.durationMs - course.remainingMs;
            for(participant of participantsDivs) {
                let resultat = new Object();
                resultat.idC = course.id;
                resultat.idA = $(participant).attr('idA');
                resultat.temps = convertMsToStr(Number($(participant).find('label').attr('nbms')));
                if(resultat.temps === undefined) {
                    resultat.temps = convertMsToStr(course.durationMs);
                }
                resultat.statut = $(participant).attr('statut');
                // console.log(resultat.duration);
                // todo if tps course % 1000 > 0 => add 1 otherwise course time might be 0 seconds
                // if an animal has not been marked as stop or abandon, not stored in the DB
                // only stored when button clicked

                console.log('before ajax call : ', resultat);

                $.ajax({
                    url: 'lib/rqInsertResultat.php',
                    method: 'POST',
                    data: {resultat: JSON.stringify(resultat)},
                    dataType: 'json',
                    async: true
                })
                .done(function(rowCount) {
                    console.log(rowCount);
                })
                .fail(function(xhr, status) {
                    console.log(console.log(xhr, status));
                })
                .always(function(){
                    console.log('Insert new result completed');
                })

            }

            //console.log(participantsDivs, nbMs);
        }

        return changingTimeStr;
    }

    // function convertMsToStr(nbMs) {
    //     let time = new Date(nbMs);
    //     let hours = time.getUTCHours();
    //     let minutes = time.getMinutes();
    //     let seconds = time.getSeconds();
    //
    //     let changingTimeStr = `${hours.toString().padStart(2, "0")}`;
    //     changingTimeStr += `:${minutes.toString().padStart(2, "0")}`;
    //     changingTimeStr += `:${seconds.toString().padStart(2, "0")}`;
    //
    //     return changingTimeStr;
    // }

    function inscrireAnimal(animal, container) {
        let btnSupprimerInscription = $('<button>', {
            type: 'button',
            text: 'Supprimer',
            class: 'btn-supprimer-inscription'
        });

        // If inscription suppressed, we add the animal back to the select list
        btnSupprimerInscription.on('click', function() {
           let idA = $(this).closest('div').attr('idA');
           let currentAnimal = listAnimaux.find(k => k.idA === idA);
           let option = $('<option>', {
               idA: currentAnimal.idA,
               nomA: currentAnimal.nomA,
               descA: currentAnimal.descA,
               espA: currentAnimal.espA,
               nomE: currentAnimal.nomE,
               nbPattesE: currentAnimal.nbPattesE,
               nationA: currentAnimal.nationA,
               nomP: currentAnimal.nomP,
               text: currentAnimal.nomA + ' : ' +
               currentAnimal.nomE +
               (currentAnimal.nomP === null ? '' : ', ' + currentAnimal.nomP)
           });
           fsInscriptions.find('#select-animaux').append(option);

           // We remove the div with the animal information
           $(this).closest('div').remove();
           // We disable the button if there are no animals registereed for the run
           let divInscriptionsCourse = fsInscriptions.find('#div-inscriptions-course');
           if(divInscriptionsCourse.children('div').length === 0) {
               divInscriptionsCourse.find('#btn-cloturer-inscriptions').prop('disabled', true);
           }
        });

        // We create the div that will contain all the animal information
        //console.log(course);
        let divAnimal = $('<div>', {
            idA: animal.idA,
            statut: statusEnum.Inscrit,
            class: 'participant'
        });
        let pAnimal = $('<p>');
        pAnimal.append(btnSupprimerInscription);
        pAnimal.append($('<span>', {text: animal.idA}));
        pAnimal.append($('<span>', {text: animal.nomA}));
        pAnimal.append($('<span>', {text: animal.nomP}));
        pAnimal.append($('<span>', {text: animal.descA}));
        pAnimal.append($('<span>', {text: animal.nomE}));

        divAnimal.append(btnSupprimerInscription);
        divAnimal.append(pAnimal);
        container.append(divAnimal);
    }

    function isCourseDataValid() {
        let error = false;
        if((txtCourseName.val().trim()).length === 0) {
            txtCourseName.siblings('.error').text('Le nom de la course ne peut être vide');
            error = true;
        }

        if(listCourseCountry.find('option:selected').attr('codeP') === undefined) {
            listCourseCountry.siblings('.error').text('Un pays doit obligatoirement être sélectionné');
            error = true;
        }

        if((txtCourseDuration.val().trim()).length === 0) {
            txtCourseDuration.siblings('.error').text('La durée de la course ne peut être vide');
            error = true;
        } else {
            let durationString = txtCourseDuration.val();
            let pattern = new RegExp('[0-9][0-9]:[0-5][0-9]:[0-5][0-9]');
            if(!durationString.match(pattern)) {
                txtCourseDuration.siblings('.error').text('La durée de la course doit respecter le format 99:59:59');
                error = true;
            } else {
                let arrayTime = durationString.split(':');
                let courseDurationMs =
                    Number(arrayTime[0])*60*60*1000 +
                    Number(arrayTime[1])*60*1000 +
                    Number(arrayTime[2]*1000);
                if(courseDurationMs < MIN_COURSE_DURATION_MS) {
                    txtCourseDuration.siblings('.error').text('La durée de la course doit être au minimum de 30 secondes');
                    error = true;
                } else {
                    course.durationMs = courseDurationMs;
                }
            }
        }

        return !error;
    }

    function loadCountryList() {
        $.ajax({
            url: 'lib/rqListePays.php',
            method: 'GET',
            async: true,
            dataType: 'json'
        })
        .done(function(data) {
            for(pays of data) {
                let option = $('<option>', {
                    codeP: pays.codeP,
                    text: pays.nomP
                });
                listCourseCountry.append(option);
            }
            btnCreateCourse.prop('disabled', false);
        })
        .fail(function(xhr, status) {
            console.log(xhr.statusText, status);
        })
        .always(function() {
            console.log('Req Liste Pays completed');
        })
    }

    //*****************************************************************************
});