//return yyyy-mm-dd if '-' is the separator selected (default)
function dateObjectToEnglishFormat(dateObject, separator = '-') {
    let obj = extractDate(dateObject);
    return obj['year'] + separator + obj['month'] + separator + obj['day'];
}

//return dd-mm-yyyy if '-' is the separator selected (default)
function dateObjectToFrenchFormat(dateObject, separator = '-') {
    let obj = extractDate(dateObject);
    return obj['day'] + separator + obj['month'] + separator + obj['year'];
}

// extract the year, month, and day of a Date Object and return
// the values inside an anonymous JS Object
function extractDate(dateObject) {
    if(!(dateObject instanceof Date)) {
        throw "dateObject must be an instance of Date";
    }
    let year = dateObject.getUTCFullYear();
    let month = dateObject.getUTCMonth() + 1;
    month = month.toString().padStart(2, '0');
    let day = dateObject.getUTCDate();
    day = day.toString().padStart(2, '0');

    return {'year': year, 'month': month, 'day': day};
}

// return the number of days between two Date Objects
function dateDiffDays(dateObj1, dateObj2) {
    if(!(dateObj1 instanceof Date && dateObj2 instanceof Date)) {
        throw "Date objects must be instances of Date";
    }
    let timeElapsed = (dateObj2 > dateObj1) ? dateObj2 - dateObj1 : dateObj1 - dateObj2;
    let nbDays = timeElapsed / (1000 * 3600 * 24);
    return Math.ceil(nbDays);
}

// Tests
function tests() {
    let date = new Date();
    let dateSql = dateObjectToEnglishFormat(date);
    console.log(dateSql);
    dateSql = dateObjectToFrenchFormat(date);
    console.log(dateSql);
    dateSql = dateObjectToFrenchFormat(date, '/');
    console.log(dateSql);
}

//tests();
