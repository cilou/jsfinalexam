$('document').ready(function() {
    //*****************************************************************************
    const MAX_COURSE_NAME_LENGTH = 30;
    const MAX_COURSE_DESC_LENGTH = 100;
    const MIN_COURSE_DURATION_MS = 5000;
    const INTERVAL_DURATION_CHRONO_MS = 40;
    const PENALTY_INCOMPLETE_COURSE = 2;
    const PENALTY_ABANDON_COURSE = 10;

    const statusEnum = {"Inscrit": "I", "EnCourse": "C", "Arrive": "T", "Abandon": "A"};
    Object.freeze(statusEnum);

    let course = new Object();
    let listAnimaux = null;

    let sectionCourse = $('#section-course');
    let txtCourseName = $('#txt-course-name');
    let listCourseCountry = $('#list-course-country');
    let txtCourseDuration = $('#txt-course-duration');
    let txtareaCourseDesc = $('#txtarea-course-desc');
    let btnCreateCourse = $('#btn-create-course');
    let fsCreateCourse = $('#fs-create-course');
    let fsInscriptions = $('#fs-inscriptions');

    // -----------------------------------------------------------------------------
    // MAIN
    // -----------------------------------------------------------------------------
    fsInscriptions.hide();
    btnCreateCourse.prop('disabled', true);
    loadCountryList();
    btnCreateCourse.on('click', createCourse);

    // -----------------------------------------------------------------------------
    // FUNCTIONS
    // -----------------------------------------------------------------------------

    /* Chargement des pays dans le select de la phase création de la course */
    function loadCountryList() {
        $.ajax({
            url: 'lib/rqListePays.php',
            method: 'GET',
            async: true,
            dataType: 'json'
        })
            .done(function(data) {
                for(pays of data) {
                    let option = $('<option>', {
                        codeP: pays.codeP,
                        text: pays.nomP
                    });
                    listCourseCountry.append(option);
                }
                btnCreateCourse.prop('disabled', false);
            })
            .fail(function(xhr, status) {
                console.log(xhr.statusText, status);
            })
            .always(function() {
                console.log('Req Liste Pays completed');
            })
    }

    /* Creation du select avec la liste des animaux */
    function createSelectAnimaux() {
        let selectAnimaux = $('<select>', {
            id: 'select-animaux'
        });
        let option = $('<option>', {
            text: 'Veuillez sélectionner un animal...',
            disabled: true,
            selected: true
        });

        selectAnimaux.append(option);

        // Verifie que la liste des animaux n'est pas égale à null avant de charger les options du select
        let timeOut = null;
        if (listAnimaux === null) {
            timeOut = setInterval(function () {
                console.log('loadAnimauxSelect tests');
                if (listAnimaux != null) {
                    clearInterval(timeOut);
                    loadAnimauxSelect(selectAnimaux);
                }
            }, 100);
        } else {
            loadAnimauxSelect(selectAnimaux);
        }

        // TODO improve
        // Active le bouton inscription lorsqu'un animal est sélectionné
        // Ne devrait être fait qu'une seule fois car, au chargement, la liste est sur
        // sélectionnez un animal...
        selectAnimaux.on('change', function() {
            let btnInscrireAnimal = fsInscriptions.find('#btn-inscrire-animal');
            btnInscrireAnimal.prop('disabled', false);
        });

        return selectAnimaux;
    }

    /* Chargement des animaux dans l'object global listAnimaux */
    function loadAnimauxData() {
        $.ajax({
            url: 'lib/rqListeAnimaux.php',
            method: 'GET',
            dataType: 'json',
            async: true
        })
        .done(function(animaux) {
            listAnimaux = animaux;
        })
        .fail(function(xhr, status) {
            console.log(xhr, status);
        })
        .always(function() {
            console.log('Load listAnimaux completed');
        })
    }

    /* Chargement du select avec les animaux depuis l'objet listAnimaux */
    function loadAnimauxSelect(selectAnimaux) {
        for(animal of listAnimaux) {
            option = $('<option>', {
                idA: animal.idA,
                nomA: animal.nomA,
                descA: animal.descA,
                espA: animal.espA,
                nomE: animal.nomE,
                nbPattesE: animal.nbPattesE,
                nationA: animal.nationA,
                nomP: animal.nomP,
                text: animal.nomA + ' : ' +
                animal.nomE +
                (animal.nomP === null ? '' : ', ' + animal.nomP)
            });

            selectAnimaux.append(option);
        }
    }

    /* Phase Inscriptions -- course details */
    function createCourseDetails() {
        let divDetailsCourse = $('<div>', {
            id: 'div-details-course',
            duration: txtCourseDuration.val(),
            idC: course.id
        });
        divDetailsCourse.append($('<p>', {text: 'nom: ' + txtCourseName.val()}));
        divDetailsCourse.append($('<p>', {text: 'pays: ' + listCourseCountry.val()}));
        divDetailsCourse.append($('<p>', {text: 'durée: ' + txtCourseDuration.val()}));
        //divDetailsCourse.append($('<p>', {text: 'description: ' + txtareaCourseDesc.val()}));

        return divDetailsCourse;
    }

    /* Création de l'interface Inscriptions */
    function createInscriptionInterface() {

        fsInscriptions.append(createCourseDetails());

        let divInscriptionsCourse = $('<div>', {id: 'div-inscriptions-course'});
        divInscriptionsCourse.append(createSelectAnimaux());

        let btnInscrireAnimal = $('<button>', {
            text: 'Inscrire',
            id: 'btn-inscrire-animal',
            disabled: true
        });

        btnInscrireAnimal.on('click', function() {
            selectAnimaux = fsInscriptions.find('#select-animaux');
            let selectedAnimal = selectAnimaux.find('option:selected');
            if(selectedAnimal.attr('idA') !== undefined) {
                let animal = listAnimaux.find(k => k.idA === selectedAnimal.attr('idA'));
                //console.log(animal);
                inscrireAnimal(animal, divInscriptionsCourse);
                selectedAnimal.remove();
                fsInscriptions.find('#btn-cloturer-inscriptions').prop('disabled', false);
            }
        });

        let btnCloturerInscriptions = $('<button>', {
            text: 'Cloturer les inscriptions',
            id: 'btn-cloturer-inscriptions',
            disabled: true
        });

        // Click on btnCloturerInscriptions
        btnCloturerInscriptions.on('click', function() {
            //TODO bonne idée de sélectionner divs dans children ou bien être plus générique avec classe ?
            let registeredAnimalDivs = fsInscriptions.find('#div-inscriptions-course').children('div');

            let btnCourseStart = $('<button>', {
                type: 'button',
                id: 'btn-course-start',
                text: 'Start',
                nbMs: course.durationMs
            });

            btnCourseStart.on('click', function() {
                course.remainingMs = course.durationMs;
                fsInscriptions.find('#div-inscriptions-course').children('div[statut]').attr('statut', statusEnum.EnCourse);
                course.timer = setInterval(function() {
                    course.remainingMs -= INTERVAL_DURATION_CHRONO_MS;
                    fsInscriptions.find('#txt-course-remaining-time').val(updateTime(course.remainingMs));
                    fsInscriptions.find('#txt-course-start-time').val(updateTime(course.durationMs - course.remainingMs));
                }, INTERVAL_DURATION_CHRONO_MS);
                $(this).prop('disabled', true);
                fsInscriptions.find('.btn-course-stop').prop('disabled', false);
            });

            $(this).siblings('#select-animaux').before(btnCourseStart);
            $(this).siblings('#select-animaux').remove();

            let txtCourseStartTime = $('<input>', {
                type: 'text',
                id: 'txt-course-start-time',
                value: '00:00:00.000',
                nbMs: 0,
                readonly: true
            });
            $(this).siblings('#btn-inscrire-animal').before(txtCourseStartTime);
            $(this).siblings('#btn-inscrire-animal').remove();

            let lblCourseRemainingTime = $('<label>', {
                for: 'txt-course-remaining-time',
                text: 'Temps restant'
            });
            let txtCourseRemainingTime = $('<input>', {
                type: 'text',
                id: 'txt-course-remaining-time',
                value: updateTime(course.durationMs),
                nbMs: course.durationMs,
                readonly: true
            });

            $(this).before(lblCourseRemainingTime);
            $(this).before(txtCourseRemainingTime);
            $(this).remove();

            $(this).prop('disabled', true);

            // We create the STOP button template
            let btnStop = $('<button>', {
                type: 'button',
                class: 'btn-course-stop',
                text: 'STOP',
                disabled: true
            });
            btnStop.on('click', function() {
                let txtCourseTime = fsInscriptions.find("#txt-course-start-time");
                let lbl = $('<label>', {
                    text: txtCourseTime.val(),
                    nbMs: course.durationMs - course.remainingMs
                });
                $(this).before(lbl);
                $(this).siblings('.btn-course-abandon').remove();
                $(this).closest('div').removeClass('running');
                $(this).closest('div').addClass('stop');
                $(this).closest('div').attr('statut', statusEnum.Arrive);
                $(this).remove();
                if(fsInscriptions.find('.running').length === 0) {
                    updateTime(0);
                    console.log('Course is finished');
                }
            });

            // We create the Abandon button template
            let btnAbandon = $('<button>', {
                type: 'button',
                class: 'btn-course-abandon',
                text: 'Abandon'
            });
            btnAbandon.on('click', function() {
                let lbl = $('<label>', {
                    text: convertMsToStrWithMs(course.durationMs* PENALTY_ABANDON_COURSE),
                    nbMs: course.durationMs * PENALTY_ABANDON_COURSE
                });
                $(this).before(lbl);
                $(this).siblings('.btn-course-stop').remove();
                $(this).closest('div').removeClass('running');
                $(this).closest('div').addClass('abandon');
                $(this).closest('div').attr('statut', statusEnum.Abandon);
                $(this).remove();
                if(fsInscriptions.find('.running').length === 0) {
                    updateTime(0);
                    console.log('Course is finished');
                }
            });

            // We add clones of the button templates created above and we remove the 'Supprimer' button
            for(div of registeredAnimalDivs) {
                $(div).addClass('running');
                $(div).find('.btn-supprimer-inscription').before(btnStop.clone(true));
                $(div).find('.btn-supprimer-inscription').before(btnAbandon.clone(true));
                $(div).find('.btn-supprimer-inscription').remove();
            }
        }); // END Click on btnCloturerInscriptions

        divInscriptionsCourse.append(btnInscrireAnimal);
        divInscriptionsCourse.append(btnCloturerInscriptions);

        return divInscriptionsCourse;
    }

    /* Crée la course dans la DB après validation des données  et
     * crée l'interface d'inscription à cette course
     */
    function createCourse() {
        let errors = sectionCourse.find('.error');
        for(error of errors) {
            $(error).empty();
        }

        if(isCourseDataValid()) {

            // Charge la liste des animaux dans le select des inscriptions (async donc on le start
            // déjà vu qu'on sait que les données entrées sont valides)
            loadAnimauxData();
            // Attribution des valeurs de la course à l'objet course
            // Limite le nombre de charactères insérés dans la DB pour certains champs
            course.nomC = (txtCourseName.val().trim()).substr(0, MAX_COURSE_NAME_LENGTH);
            course.descC = (txtareaCourseDesc.val().trim()).substr(0, MAX_COURSE_DESC_LENGTH);
            let optionSelected = listCourseCountry.find('option:selected');
            course.lieuC = optionSelected.attr('codeP');
            course.nomPays = optionSelected.text();
            course.dateC = dateObjectToEnglishFormat(new Date());

            // Insertion de la course dans la DB
            $.ajax({
               url: 'lib/rqInsertCourse.php',
               method: 'POST',
               dataType: 'json',
               data: {course: JSON.stringify(course)}
            })
            .done(function(lastInsertedId) {
                if(lastInsertedId === 0) {
                    btnCreateCourse.siblings('.error')
                        .text("Une erreur s'est produite lors de l'insertion des données. Veuillez réessayer plus tard.");
                } else {
                    course.id = lastInsertedId;

                    txtCourseName.prop('disabled', true);
                    listCourseCountry.prop('disabled', true);
                    txtCourseDuration.prop('disabled', true);
                    txtareaCourseDesc.prop('disabled', true);
                    btnCreateCourse.prop('disabled', true);

                    // Création de l'interface inscriptions
                    fsInscriptions.append(createInscriptionInterface());

                    fsCreateCourse.remove();
                    fsInscriptions.show();
                }
            })
            .fail(function(xhr, status) {
                console.log(xhr, status);
            })
            .always(function(){
                console.log('Insert Course completed');
            });
        }
    }

    function updateTime(nbMs) {
        changingTimeStr = convertMsToStrWithMs(nbMs);

        if(nbMs <= 0 && course.timer !== null) {
            clearInterval(course.timer);
            course.timer = null;

            let participantsDivsStillRunning = fsInscriptions.find('.running');
            for(participant of participantsDivsStillRunning) {
                $(participant).find('.btn-course-stop').trigger("click");
                $(participant).find('label').attr('nbms', course.durationMs*PENALTY_INCOMPLETE_COURSE);
                $(participant).find('label').text(convertMsToStrWithMs(Number(course.durationMs*PENALTY_INCOMPLETE_COURSE)));
                $(participant).attr('statut', statusEnum.Arrive);
            }

            let participantsDivs = fsInscriptions.find('.participant');
            for(participant of participantsDivs) {
                let resultat = new Object();
                resultat.idC = course.id;
                resultat.idA = $(participant).attr('idA');
                resultat.temps = convertMsToStr(Number($(participant).find('label').attr('nbms')));
                resultat.statut = $(participant).attr('statut');
                // todo if tps course % 1000 > 0 => add 1 otherwise course time might be 0 seconds

                $.ajax({
                    url: 'lib/rqInsertResultat.php',
                    method: 'POST',
                    data: {resultat: JSON.stringify(resultat)},
                    dataType: 'json',
                    async: true
                })
                .done(function(rowCount) {
                    console.log(rowCount);
                })
                .fail(function(xhr, status) {
                    console.log(console.log(xhr, status));
                })
                .always(function(){
                    console.log('Insert new result completed');
                })
            }
        }

        return changingTimeStr;
    }

    function inscrireAnimal(animal, container) {
        let btnSupprimerInscription = $('<button>', {
            type: 'button',
            text: 'Supprimer',
            class: 'btn-supprimer-inscription'
        });

        // If inscription suppressed, we add the animal back to the select list
        btnSupprimerInscription.on('click', function() {
           let idA = $(this).closest('div').attr('idA');
           let currentAnimal = listAnimaux.find(k => k.idA === idA);
           let option = $('<option>', {
               idA: currentAnimal.idA,
               nomA: currentAnimal.nomA,
               descA: currentAnimal.descA,
               espA: currentAnimal.espA,
               nomE: currentAnimal.nomE,
               nbPattesE: currentAnimal.nbPattesE,
               nationA: currentAnimal.nationA,
               nomP: currentAnimal.nomP,
               text: currentAnimal.nomA + ' : ' +
               currentAnimal.nomE +
               (currentAnimal.nomP === null ? '' : ', ' + currentAnimal.nomP)
           });
           fsInscriptions.find('#select-animaux').append(option);

           // We remove the div with the animal information
           $(this).closest('div').remove();
           // We disable the button if there are no animals registereed for the run
           let divInscriptionsCourse = fsInscriptions.find('#div-inscriptions-course');
           if(divInscriptionsCourse.children('div').length === 0) {
               divInscriptionsCourse.find('#btn-cloturer-inscriptions').prop('disabled', true);
           }
        });

        // We create the div that will contain all the animal information
        let divAnimal = $('<div>', {
            idA: animal.idA,
            statut: statusEnum.Inscrit,
            class: 'participant'
        });
        let pAnimal = $('<p>');
        pAnimal.append(btnSupprimerInscription);
        pAnimal.append($('<span>', {text: animal.idA}));
        pAnimal.append($('<span>', {text: animal.nomA}));
        pAnimal.append($('<span>', {text: animal.nomP}));
        pAnimal.append($('<span>', {text: animal.descA}));
        pAnimal.append($('<span>', {text: animal.nomE}));

        divAnimal.append(btnSupprimerInscription);
        divAnimal.append(pAnimal);
        container.append(divAnimal);
    }

    function isCourseDataValid() {
        let error = false;
        if((txtCourseName.val().trim()).length === 0) {
            txtCourseName.siblings('.error').text('Le nom de la course ne peut être vide');
            error = true;
        }

        if(listCourseCountry.find('option:selected').attr('codeP') === undefined) {
            listCourseCountry.siblings('.error').text('Un pays doit obligatoirement être sélectionné');
            error = true;
        }

        if((txtCourseDuration.val().trim()).length === 0) {
            txtCourseDuration.siblings('.error').text('La durée de la course ne peut être vide');
            error = true;
        } else {
            let durationString = txtCourseDuration.val();
            let pattern = new RegExp('[0-9][0-9]:[0-5][0-9]:[0-5][0-9]');
            if(!durationString.match(pattern)) {
                txtCourseDuration.siblings('.error').text('La durée de la course doit respecter le format 99:59:59');
                error = true;
            } else {
                let arrayTime = durationString.split(':');
                let courseDurationMs =
                    Number(arrayTime[0])*60*60*1000 +
                    Number(arrayTime[1])*60*1000 +
                    Number(arrayTime[2]*1000);
                if(courseDurationMs < MIN_COURSE_DURATION_MS) {
                    txtCourseDuration.siblings('.error')
                        .text(`La durée de la course doit être au minimum de ${MIN_COURSE_DURATION_MS/1000} secondes`);
                    error = true;
                } else {
                    course.durationMs = courseDurationMs;
                }
            }
        }

        return !error;
    }

    //*****************************************************************************
});